"""
Bootp script implement a basic bootp server for assigning ip addresses to devices that support the bootp protocol.
see `RFC1532 <https://tools.ietf.org/html/rfc1532>`_ and see `RFC1542 <https://tools.ietf.org/html/rfc1542>`_ for specification.

Examples:
    .. code-block:: python

        import time
        from bootp import BootpServer

        bp = BootpServer()
        bp.set_device("00-A0-EC-44-9B-2E", "192.168.0.22")
        bp.start()

        # wait for client to be serviced
        while bp.device_serviced("00-A0-EC-44-9B-2E") is None:
            time.sleep(1)
        time.sleep(5)  # let device take ip

        # user code here

        bp.stop()

"""
from __future__ import print_function
import socket
import time
import datetime
import struct
import ctypes
from ctypes import sizeof, c_char, c_ubyte, c_uint16, c_uint32, c_uint64, BigEndianStructure
import os
import threading

__all__ = ["BootpServer", "Device"]

BOOTP_CLIENT_PORT = 68
BOOTP_SERVER_PORT = 67

class NetworkStructure(BigEndianStructure):
    _pack_ = 1

    def pack(self):
        return memoryview(self).tobytes()

    def unpack(self, bytes):
        fit = min(len(bytes), ctypes.sizeof(self))
        ctypes.memmove(ctypes.addressof(self), bytes, fit)

class BootpStructure(NetworkStructure):
    CHADDR_LEN = 16
    SNAME_LEN  = 64
    FILE_LEN   = 128
    VEND_LEN   = 64

    _fields_  = [
        ("op",     c_ubyte),
        ("htype",  c_ubyte),
        ("hlen",   c_ubyte),
        ("hops",   c_ubyte),
        ("xid",    c_uint32),
        ("secs",   c_uint16),
        ("unused", c_uint16),
        ("ciaddr", c_uint32),
        ("yiaddr", c_uint32),
        ("siaddr", c_uint32),
        ("giaddr", c_uint32),
        ("chaddr", c_ubyte * CHADDR_LEN),
        ("sname",  c_char * SNAME_LEN),
        ("file",   c_char * FILE_LEN),
        ("vend",   c_ubyte * VEND_LEN),

    ]


class VendTag(NetworkStructure):
    _fields_  = [
        ("tag",  c_ubyte),
        ("len",  c_ubyte),
    ]

class VendNode(NetworkStructure):
    _fields_  = [
        ("pre",      VendTag),
        ("payload",  c_uint32),
    ]

class VendHeader(NetworkStructure):
    _fields_  = [
        ("magic",  c_uint32),
        ("pad",    c_ubyte),
    ]


def list_networks(ipv4_only=False):
    ifc = socket.getaddrinfo(socket.gethostname(), None)
    if ipv4_only:
        return [i[4][0] for i in ifc if i[0] == socket.AF_INET]
    return [i[4][0] for i in ifc]

def ip2int(addr):
    return struct.unpack("!I", socket.inet_aton(addr))[0]


def int2ip(addr):
    return socket.inet_ntoa(struct.pack("!I", addr))


class Device(object):
    """
    Data storage class for client devices.

    Attributes:
        mac (str): mac address of client "00:A0:EC:44:9B:2E"
        ip (str): ip address of client "192.168.0.22"
        subnet (str): subnet mask for client "255.255.255.0"
        gateways (list[str]): gateway address for client
        time_seen (datetime.datetime): time of last request
    """

    def __init__(self):
        self.mac       = None
        self.ip        = None
        self.subnet    = None
        self.gateways  = None
        self.time_seen = None


class BootpServer(object):
    """
    Bootp Server class for Handlering Bootp requests

    """

    def __init__(self, interfaces=None):
        self.devices        = {}
        self.devices_served = {}
        self.file_name      = {bytes():bytes()}

        self._sockets       = []
        self._listen_thread = None
        self._listen_active = False

        self._interfaces = interfaces
        if self._interfaces is None:
            self._interfaces = list_networks(True)

    def interfaces(self):
        """
        Returns: Gives list of strings for ip address of interfaces that are being listen on.
        """
        return self._interfaces

    def device_serviced(self, mac=None):
        """
        gets list of devices that have had bootp addresses assigned.

        Args:
            mac(str): optional if used then only device object for mac address provided is returned

        Returns(dict(str, Device)): gives dictionary of Device objects against mac addresses.

        """
        if mac is None:
            return self.devices_served
        return self.devices_served.get(standardise_mac(mac), None)

    def set_device(self, mac, ip, subnet=None, gateways=[]):
        """
        Set a device to be configured via bootp when a request packet is received.

        Args:
            mac(str): mac address of device to be serviced "00-A0-EC-44-9B-2E"
            ip(str): ip address for the device to take "192.168.0.22"
            subnet(str): optional "255.255.255.0"
            gateways(str): optional "192.168.0.1"

        Returns: None
        """

        mac = standardise_mac(mac)

        if isinstance(ip, str):
            ip = ip2int(ip)

        if isinstance(subnet, str):
            subnet = ip2int(subnet)

        parsed_gates = []
        for g in gateways:
            if isinstance(g, str):
                parsed_gates.append(ip2int(g))

        device            = Device()
        device.mac        = mac
        device.ip         = ip
        device.subnet     = subnet
        device.gateways   = parsed_gates
        self.devices[mac] = device

    def start(self):
        """
        Starts Bootp server. Calling start again will restart the server.

        :return: None
        """
        # if called a second time close all sockets and stop thread
        if self._listen_thread is not None:
            self._listen_active = False
            self._listen_thread.join()

        for s in self._sockets:
            s.close()

        self._sockets = []

        # start sockets on interfaces to listen
        for ifc in self._interfaces:
            listen = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            listen.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            listen.setblocking(False)
            listen.bind((ifc, BOOTP_SERVER_PORT))
            self._sockets.append(listen)

        self._listen_active = True
        self._listen_thread = threading.Thread(target=self._listen)
        self._listen_thread.start()

    def stop(self):
        """
        Stops Bootp server

        :return: None
        """
        self._listen_active = False
        self._listen_thread.join()


    def _listen(self):
        """
        Runs in own thread listing for Bootp requests and processing them.
        Returns: None
        """

        while self._listen_active:
            for sock in self._sockets:
                rcv_len = 0
                request   = BootpStructure()
                try:
                    rcv_len = sock.recv_into(request, sizeof(BootpStructure))
                    ifc     = sock.getsockname()[0]
                except Exception as e:
                    pass

                if rcv_len == sizeof(BootpStructure):
                    self._process_request(request, ifc)

    def _process_request(self, request, ifc):
        """
        Parses Bootp requests and replys if valid.
        Args:
            request(BootpStructure):
            ifc(str): ip address of interface that received request.
        Returns: True is response is sent

        """

        responce = BootpStructure()
        responce.op     = 2 # Reply
        responce.hlen   = 6 # Hardware address length ether net is 6
        responce.htype  = 1 # Ethernet
        responce.siaddr = ip2int(ifc)
        responce.xid    = request.xid # transaction id
        responce.ciaddr = request.ciaddr # client propused ip address
        responce.secs   = request.secs #  time since list request sent
        responce.sname  = bytes(os.name, encoding='ascii')
        dest_addr       = b"255.255.255.255"

        if request.op != 1:  # is this a REQUEST
            return False

        if request.htype != 1:  # only supports ethernet
            return False

        if request.hlen != 6:  # only support mac addresses of length 6
            return False

        for i, byt in enumerate(request.chaddr):  # copy clients hardware address
            responce.chaddr[i] = byt

        if request.ciaddr == 0:  # if client has no proposed address set one
            mac = mac_array_to_str(request.chaddr[:request.hlen])
            device = self.devices.get(mac, None) #type: Device
            if device is None:
                device = Device()
                device.mac = mac
                device.time_seen = datetime.datetime.now()
                self.devices_served[mac] = device
                return False
            responce.yiaddr = device.ip
        else:
            mac = mac_array_to_str(request.chaddr[:request.hlen])
            device = self.devices.get(mac, Device())  # type: Device
            dest_addr = int2ip(request.ciaddr)
            device.mac = mac
            device.ip  = dest_addr  # todo: this needs checking as it looks like it should set responce.yiaddr with found value
            device.time_seen = datetime.datetime.now()

        if request.file != "":  # if a file is request and we have have it send it.
            file_name = self.file_name.get(request.file, None)
            if file_name is None:
                return False
            responce.file = file_name

        # check the start of vend for the magic cookie
        if  request.vend[0] == 99  \
        and request.vend[1] == 130 \
        and request.vend[2] == 83  \
        and request.vend[3] == 99:
            data = self._parse_magic_cookie(request)
            if data is not None:
                fit = min(len(data), ctypes.sizeof(responce.vend))
                ctypes.memmove(ctypes.addressof(responce.vend), data, fit)
            else:
                return False

        self._send_on(responce.pack(), dest_addr, ifc)
        device.time_seen = datetime.datetime.now()
        self.devices_served[mac] = device

        return True

    def _parse_magic_cookie(self, request):
        # see RFC 1395 for magic cookie format https://tools.ietf.org/html/rfc1395

        mac = mac_array_to_str(request.chaddr[:request.hlen])
        device = self.devices.get(mac, None)

        if device is None:
            return None

        data_out = VendHeader(ip2int('99.130.83.99'), 0).pack()
        for i in range(6, 6 + request.vend[5]):
            parameter = request.vend[i]

            if parameter == 1:  # subnet request
                data_out += VendNode((parameter, 4), device.subnet).pack()
            if parameter == 2:  # time request
                data_out += VendNode((parameter, 4), int(time.time())).pack()
            if parameter == 3 and len(device.gateways): # gateway request
                data_out += VendTag(parameter, len(device.gateways) * 4).pack()
                Gateways = c_uint32 * len(device.gateways)
                gateways = Gateways(*(device.gateways[::-1]))
                data_out += memoryview(gateways).tobytes()[::-1]

        data_out += memoryview(c_ubyte(255)).tobytes()  # add end tag

        return data_out


    def _send_on(self, data, to, interface):
        for s in self._sockets:
            if interface == s.getsockname()[0]:
                s.sendto(data, (to, BOOTP_CLIENT_PORT))

def standardise_mac(mac):
    if isinstance(mac, list):
        mac = mac_array_to_str(mac)
    return mac.replace("-", ":").upper()

def mac_array_to_str(arr):
    if isinstance(arr, str):
        return ":".join("{:02x}".format(ord(c)) for c in arr).upper()
    return ":".join("{:02x}".format(c) for c in arr).upper()

def bytes_to_hex(arr):
    if isinstance(arr, str):
        return ":".join("{:02x}".format(ord(c)) for c in arr).upper()
    return ":".join("{:02x}".format(c) for c in arr).upper()


def print_table(rows, header=None, header_sep='-', column_sep='| ', return_table=False, adjust='left'):
    """
    Prints and ascii table with the data held in rows. all data items must be capable of converting to string.

    Args:
        rows: list of rows each containing list of coloumns of data [["r1c1", "r1c2"],["r2c1", "r2c2"]]
        header: list of column names.
        header_sep: header separater character
        column_sep: column separater character.
        return_table: if false then table is printed if true tables string is returned.
        adjust: adjustment of cells "left"|"centre"|"right"

    Returns: None

    """

    if header is not None:
        col_widths = [len(str(col)) for col in header]
    else:
        max_cols = max(*[len(row) for row in rows])
        col_widths = [1] * max_cols

    for row in rows:
        for i, col in enumerate(row):
            col_widths[i] = max(col_widths[i], len(str(col)))

    table_width = sum(col_widths) + (len(column_sep) * (len(col_widths) + 1))

    if header is not None:
        rows.insert(0, header)

    text_rows = [column_sep] * len(rows)
    for i, row in enumerate(rows):
        for width, col in zip(col_widths, row):
            if adjust == 'left':
                text_rows[i] += str(col).ljust(width, ' ')
            elif adjust == 'right':
                text_rows[i] += str(col).rjust(width, ' ')
            elif adjust == 'centre':
                text_rows[i] += str(col).center(width, ' ')
            else:
                raise Exception("adjust can only be left, right, centre")

            text_rows[i] += str(column_sep)

    if header is not None and header_sep is not None:
        sep_qty = table_width//len(header_sep)
        header_sep_text = sep_qty * header_sep
        text_rows.insert(1, header_sep_text)

    raw_text = os.linesep.join(text_rows)

    if return_table:
        return raw_text

    print(raw_text)

def console():
    """
    if this is the main entry point then run script with a cli interface
    """
    import cmd
    import argparse

    class interpreter(cmd.Cmd):
        prompt = '> '
        def do_add(self, line):
            """
            add device to be serviced format MAC IP SUBNET: 00-A0-EC-44-9B-2E 192.168.0.15 255.255.255.0
            """
            try:
                mac, ip, sub = line.split(' ')
            except:
                print("add must be passed mac ip sub:\n\t00-A0-EC-44-9B-2E 192.168.0.15 255.255.255.0")
                return
            bp.set_device(mac, ip, sub)
            print("Device added")

        def do_show(self, line):
            """
            shows list of devices seen
            """
            rows = []
            for device in bp.device_serviced().values():
                rows.append([device.mac, int2ip(device.ip), int2ip(device.subnet), device.time_seen])
            print_table(rows, ("MAC", "IP", "SUBNET", "TIME"))

        def do_interfaces(self, line):
            """
            Shows interfaces ip addresses that are being listen on
            """
            print_table([[ifc, ] for ifc in bp.interfaces()], ["Interface IP"])

        def do_EOF(self, line):
            return True

    parser = argparse.ArgumentParser(description="Light weight BootP server")
    parser.add_argument('-i', '--interface', nargs='?', default=None, help='Set interface to use (ip of interface)')
    args = parser.parse_args()

    if args.interface is not None:
        bp = BootpServer([args.interface])
    else:
        bp = BootpServer()

    bp.start()
    interpreter().cmdloop("BootP Started")
    bp.stop()


if __name__ == "__main__":
    console()