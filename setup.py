from setuptools import setup

setup(
        name='bootp',
        version='0.0.1',
        packages=[
            'bootp',
        ],
        entry_points={
            'console_scripts': [
                'pybootp=bootp.bootp:console',
            ],
        },
        url='',
        license='No permission',
        author='cmpye',
        author_email='cpchrispye@hotmail.com',
        description='Small Bootp server with minimal support for magic cookies',
        install_requires=[],
)
