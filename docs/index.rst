.. bootp documentation master file, created by
   sphinx-quickstart on Sat Jul  7 13:12:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bootp's documentation!
=================================

bootp python module provides a bootp server for configuring client devices network interfaces.
this module provides a class ``BootpServer`` that 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   module/bootp



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
